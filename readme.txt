/* $Id*/

Summary
-------
SugaronDrupal is a set of modules allowing the integration of both applications,
Drupal and SugarCRM using web services. There are two main reasons to have an
active web site (the public part) integrated with the CRM office backend (the
private part):

 Lead grabbing, marketing and campaign tracking: Mostly of the data is obtained
 in the Drupal site and populated to the CRM blindly for the user. Information
 from SugarCRM is obtained only in a set of configuration operations to prepare
 the Drupal site.

 Self Service portal. Most of the information is fetched by Drupal to be shown
 to the user, and optionally this information can be modified using Drupal to be
 updated in SugarCRM.

Both could, of course, converge into a single one. This module set will provide
those functionalities for any Drupal site.


SugarCRM supported versions
---------------------------
Currently SugaronDrupal only supports SugarCRM versions greater than 5.5 (any of
the SugarCRM flavors including Web Services API Version 2).

The form collector challenge
----------------------------
Please, realize that if you just want to have a form in your Drupal website to
grab user information these two modules may be of help.

Webform2Sugar  http://drupal.org/project/webform2sugar
SugarWebform   http://drupal.org/project/sugarwebform

Both require webform: http://drupal.org/project/webform

Requeriments
------------
No additional Drupal requirements.


Installation
------------
Copy module folder to your module directory and then enable on the admin modules
page. Go to Configuration section in Administration menu and you will find a new
group of management entries under the name SugarCRM Integration.


Configuration
-------------
Configure the SugarCRM end point in the connection settings. Currently, SugarCRM
has a bug in the 'PHP serialization' encoding method, so please, use JSON as
encoded method instead. SOAP services are not supported yet.

Once you have completed and saved your configuration options, click the 'Update
authenticate session' button in the same page. This will initialize the Session
id used during the communication.


Author
------
Iñaki López
inaki.lopez@gmail.com

Author may be contacted for module support services.
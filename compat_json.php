<?php
/**
 * @file
 * compat_json.php
 *
 * Implements simple json encoding for REST services
 *
 * Abstract from: backwards compatibility for older PHP interpreters
 * version:	14
 * license:	Public Domain
 *
 * You get PHP version independence by including() this script. It provides
 * downwards compatibility to older PHP installations by emulating missing
 * functions or constants using IDENTICAL NAMES. So this doesn't slow down
 * script execution on setups where the native functions already exist. It
 * is meant as quick drop-in solution, and to free you from rewriting or
 * cumbersome workarounds - instead of using the real & more powerful funcs.
 *
 * It cannot mirror PHP5s extended OO-semantics and functionality into PHP4
 * however. A few features are added here that weren't part of PHP yet. And
 * some other function collections are separated out into the ext/ directory.
 *
 * And further this is PUBLIC DOMAIN (no copyright, no license, no warranty)
 * so threrefore compatible to ALL open source licenses. You could rip this
 * paragraph out to republish this instead only under more restrictive terms
 * or your favorite license (GNU LGPL/GPL, BSDL, MPL/CDDL, Artist, PHPL, ...)
 *
 *
 * @emulated
 *    json_encode
 *    json_decode
 *
 */

/**
 * @since unknown
 */
if (!defined("E_RECOVERABLE_ERROR")) { define("E_RECOVERABLE_ERROR", 4096); }

/**
 * Converts PHP variable or array into "JSON" (a JavaScript value expression
 * or "object notation").
 *
 * @compat
 *    output seems identical to PECL versions
 * @bugs
 *    doesn't take care with unicode too much
 *
 */
if (!function_exists("json_encode")) {
  function json_encode($var, /* emu_args */$obj = FALSE) {

    // Prepare JSON string
    $json = "";

    // Add array entries
    if (is_array($var) || ($obj = is_object($var))) {

      // Check if array is associative
      if (!$obj) foreach ((array)$var as $i => $v) {
        if (!is_int($i)) {
          $obj = 1;
          break;
        }
      }

      // Concat invidual entries
      foreach ((array)$var as $i => $v) {
        // comma separators
        $json .= ($json ? "," : "")
       // assoc prefix
              . ($obj ? ("\"$i\":") : "")
        // value
              . (json_encode($v));
      }

      // Enclose into braces or brackets
      $json = $obj ? "{" . $json . "}" : "[" . $json . "]";
    }
    // Strings need some care
    elseif (is_string($var)) {
      if (!utf8_decode($var)) {
        $var = utf8_encode($var);
      }
      $var = str_replace(array("\"", "\\", "/", "\b", "\f", "\n", "\r", "\t"), array("\\\"", "\\\\", "\\/", "\\b", "\\f", "\\n", "\\r", "\\t"), $var);
      $json = '"' . $var . '"';
    }
    // Basic types
    elseif (is_bool($var)) {
      $json = $var ? "true" : "false";
    }
    elseif ($var === NULL) {
      $json = "null";
    }
    elseif (is_int($var) || is_float($var)) {
      $json = "$var";
    }
    // something went wrong
    else {
      trigger_error(check_plain("json_encode: don't know what a '" . gettype($var) . "' is."), E_USER_ERROR);
    }

    return $json;
  }
}



/**
 * Parses JSON (JavaScript value expression) into PHP variable
 * (array or object).
 *
 * @compat
 *    behaves similiar to PECL version
 *    but is less quiet on errors
 *    might ignore some misformed representations
 * @bugs
 *    doesn't decode unicode \uXXXX string escapes
 *
 */
if (!function_exists("json_decode")) {
  function json_decode($json, $assoc = FALSE, /* emu_args */$n = 0, $state = 0, $waitfor = 0) {

    // Result var
    $val = NULL;
    static $lang_eq = array("true" => TRUE, "false" => FALSE, "null" => NULL);
    static $str_eq = array("n" => "\012", "r" => "\015", "\\" => "\\", '"' => '"', "f" => "\f", "b" => "\b", "t" => "\t", "/" => "/");

    // Flat char-wise parsing
    for (/* n */; $n < drupal_strlen($json); /* n */) {
      $c = $json[$n];

      // in-string
      if ($state==='"') {
        if ($c == '\\') {
          $c = $json[++$n];
          if (isset($str_eq[$c])) {
            $val .= $str_eq[$c];
          }
          elseif ($c == "u") {
            $val .= "\\u";
          }
          else {
            $val .= "\\" . $c;
          }
        }
        elseif ($c == '"') {
          $state = 0;
        }
        else {
          $val .= $c;
        }
      }
      // End of sub-call (array/object)
      elseif ($waitfor && (strpos($waitfor, $c) !== FALSE)) {
        return array($val, $n);  // return current value and state
      }
      // In-array
      elseif ($state===']') {
        list($v, $n) = json_decode($json, 0, $n, 0, ",]");
        $val[] = $v;
        if ($json[$n] == "]") {
          return array($val, $n);
        }
      }
      // In-object
      elseif ($state==='}') {
        list($i, $n) = json_decode($json, 0, $n, 0, ":");   // this allowed non-string indicies
        list($v, $n) = json_decode($json, 0, $n+1, 0, ",}");
        $val[$i] = $v;
        if ($json[$n] == "}") {
          return array($val, $n);
        }
      }
      // looking for next item (0)
      else {
        // Whitesapce
        if (preg_match("/\s/", $c)) {
          // skip
        }
        // String begin
        elseif ($c == '"') {
          $state = '"';
        }
        // Object
        elseif ($c == "{") {
          list($val, $n) = json_decode($json, $assoc, $n+1, '}', "}");
          if ($val && $n && !$assoc) {
            $obj = new stdClass();
            foreach ($val as $i => $v) {
              $obj->{$i} = $v;
            }
            $val = $obj;
            unset($obj);
          }
        }
        // array
        elseif ($c == "[") {
          list($val, $n) = json_decode($json, $assoc, $n+1, ']', "]");
        }
        // Comment
        elseif (($c == "/") && ($json[$n+1]=="*")) {
          // just find end, skip over
          ($n = strpos($json, "*/", $n+1)) or ($n = drupal_strlen($json));
        }
        // Numbers
        elseif (preg_match("#^(-?\d+(?:\.\d+)?)(?:[eE](-?\d+))?#", drupal_substr($json, $n), $uu)) {
          $val = $uu[1];
          $n += drupal_strlen($uu[0]) - 1;
          $val = strpos($val, ".") ? (float)$val : (int)$val;
          if (isset($uu[2])) {
            $val *= pow(10, (int)$uu[2]);
          }
        }
        // boolean or null
        elseif (preg_match("#^(true|false|null)\b#", drupal_substr($json, $n), $uu)) {
          $val = $lang_eq[$uu[1]];
          $n += drupal_strlen($uu[1]) - 1;
        }
        // parsing error
        else {
          // PHPs native json_decode() breaks here usually and QUIETLY
          trigger_error(check_plain("json_decode: error parsing '$c' at position $n"), E_USER_WARNING);
          return $waitfor ? array(NULL, 1<<30) : NULL;
        }

      } //state

      // Next char
      if ($n === NULL) {
        return NULL;
      }
      $n++;
    } //for

    return ($val);
  }
}

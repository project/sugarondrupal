<?php

/**
 * @file
 * sugarondrupal.admin.inc
 *
 * provides administrative pages
 *
 */

/**
 * Menu function to show a SugarCRM information category.
 *
 * Mimic the way Drupal generates a Menu block page.
 */
function sugarondrupal_system_admin_menu_block_page() {
  module_load_include('inc', 'system', 'system.admin');

  return system_admin_menu_block_page();
}

/**
 * Builds the SugarCRM connection configuration form.
 *
 * This form configures a default SugarCRM endpoint.
 *
 * The form contains enough information to make a services connection request
 * and the connection networking settings. The only required attributes are the
 * connection settings. The authentication information will only be saved when
 * the 'Save settings' is clicked. Clicking the 'Update session' button will
 * make a login request with the credentials submitted in the form.
 *
 * @return
 *   Settings form array.
 */
function sugarondrupal_admin_settings() {
  $form = array();

  /**
   * Web service information includes protocol settings. Currently only version
   * 2 of SugarCRM services API and REST protocol are supported.
   */
  $form['sugarondrupal_webservice'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('SugarCRM Web Service End Point'),
    '#required'       => TRUE,
    '#description'    => t('Configure the SugarCRM connection protocol settings.'),
  );
  $form['sugarondrupal_webservice']['sugarondrupal_webservice_endpoint'] = array(
    '#type'           => 'textfield',
    '#title'          => t('SugarCRM End Point'),
    '#size'           => 65,
    '#maxlength'      => 256,
    '#required'       => TRUE,
    '#default_value'  => variable_get('sugarondrupal_webservice_endpoint', NULL),
    '#description'    => t('Enter the URL of your SugarCRM web service. Valid examples are: http://server/sugarcrm/service/v2/rest.php, http://server/service/v2/soap.php?wsdl.'),
  );

  // Ignore SOAP for now. The version 2 services API has different responses if
  // they are invoked using SOAP protocol.
  $protocols = array('rest' => 'REST protocol');
  $form['sugarondrupal_webservice']['sugarondrupal_webservice_protocol'] = array(
    '#type'           => 'radios',
    '#title'          => t('SugarCRM Web Service Protocol'),
    '#options'        => $protocols,
    '#required'       => TRUE,
    '#default_value'  => variable_get('sugarondrupal_webservice_protocol', 'rest'),
    '#description'    => t('Choose if the Web Service End Point is a SOAP or RESTful handler. REST services are only available from SugarCRM versions >= 5.5'),
  );

  // When json PECL extension is not enabled in PHP, we provide our custom
  // compat json encoder.
  module_load_include('php', 'sugarondrupal', 'compat_json');
  $encodings = array(
    'JSON'      => t('JSON encoding'),
    'SERIALIZE' => t('PHP serialization'),
  );

  $form['sugarondrupal_webservice']['sugarondrupal_rest_encoding'] = array(
    '#type'           => 'radios',
    '#title'          => t('SugarCRM RESTful encoding'),
    '#options'        => $encodings,
    '#required'       => TRUE,
    '#default_value'  => variable_get('sugarondrupal_rest_encoding', 'JSON'),
    '#description'    => t('If the Web Service End Point is a RESTful service, select the type of encoding for the communication.'),
  );

  // Debug option.
  $form['sugarondrupal_webservice']['sugarondrupal_debug'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Debug information'),
    '#default_value'  => variable_get('sugarondrupal_debug', FALSE),
    '#description'    => t('Record debug information using Watchdog. Debug information can affect the site performance. Enable this option only if you are experiencing errors or unexpected behavior.'),
  );

  /**
   * SugarCRM advanced settings. Advanced settings provide support for the HTTP
   * connection using the apropiate parameters to drupal_http_request for max
   * redirects, timeout and headers. Additionally, SugaronDrupal provides a way
   * to easily setup HTTP authentication credentials.
   */
  $form['sugarondrupal_advanced'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Advanced settings'),
    '#collapsible'    => TRUE,
    '#collapsed'      => TRUE,
    '#description'    => t('Advanced connection options.'),
  );
  $form['sugarondrupal_advanced']['sugarondrupal_http_auth_user'] = array(
    '#type'           => 'textfield',
    '#title'          => t('HTTP Authentication user name'),
    '#default_value'  => variable_get('sugarondrupal_http_auth_user', ''),
  );
  $form['sugarondrupal_advanced']['sugarondrupal_http_auth_pass'] = array(
    '#type'           => 'textfield',
    '#title'          => t('HTTP Authentication password'),
    '#default_value'  => variable_get('sugarondrupal_http_auth_pass', ''),
    '#description'    => t('If your SugarCRM site requires HTTP authentication (Basic realm) you can provide valid user name and password credentials in these fields.'),
  );
  $form['sugarondrupal_advanced']['sugarondrupal_http_redirects'] = array(
    '#type'           => 'textfield',
    '#size'           => 5,
    '#title'          => t('HTTP connection redirects'),
    '#default_value'  => variable_get('sugarondrupal_http_redirects', '3'),
    '#description'    => t('how many times a redirect instruction from the HTTP server may be followed.'),
  );
  $form['sugarondrupal_advanced']['sugarondrupal_http_timeout'] = array(
    '#type'           => 'textfield',
    '#size'           => 5,
    '#title'          => t('Connection timeout'),
    '#default_value'  => variable_get('sugarondrupal_http_timeout', '30.0'),
    '#description'    => t('Maximum number of seconds to wait before dropping the connection.'),
  );

  /**
   * SugarCRM Authentication settings. They are separated from endpoint because
   * this settings are not required to be saved or stored for the module to
   * work. A Username and password can be provided only to authenticate the
   * session ID.
   */
  $form['sugarondrupal_authentication'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('SugarCRM Authentication'),
    '#collapsible'    => variable_get('sugarondrupal_pass', ''),
    '#collapsed'      => variable_get('sugarondrupal_pass', ''),
    '#description'    => t('The user and password of these fields should have access to some of the modules and information of your SugarCRM setup. Read carefully the documentation about how to prepare a secure user for this integration.'),
  );
  $form['sugarondrupal_authentication']['sugarondrupal_user'] = array(
    '#type'           => 'textfield',
    '#title'          => t('User login'),
    '#size'           => 32,
    '#maxlength'      => 64,
    '#default_value'  => variable_get('sugarondrupal_user', ''),
    '#description'    => t('Enter the login name of the account used to connect Drupal with the SugarCRM installation'),
  );
  $form['sugarondrupal_authentication']['sugarondrupal_pass'] = array(
    '#type'           => 'password',
    '#title'          => t('Password'),
    '#size'           => 32,
    '#maxlength'      => 64,
    '#default_value'  => variable_get('sugarondrupal_pass', ''),
    '#description'    => t('Enter the password of the account'),
  );

  /**
   * Actions section.
   */

  // Include the 'Update authenticated session' button in the form Actions.
  $form['actions']['update_session_id'] = array(
    '#type'           => 'submit',
    '#value'          => 'Update authenticated session',
    '#submit'         => array('sugarondrupal_admin_settings_update_session_submit'),
    '#weight'         => 10,
  );

  return system_settings_form($form);
}


/**
 * Validator hook for settings form.
 *
 * Verify that endpoint is not a SOAP endpoint.
 *
 * @todo: remove the SOAP endpoint check when SOAP is supported.
 */
function sugarondrupal_admin_settings_validate($form, &$form_state) {
  // SOAP is not supported for now.
  if (!strtok('?wsdl', $form_state['values']['sugarondrupal_webservice_endpoint'])) {
    form_set_error('sugarondrupal_webservice_endpoint', t('SOAP Service End point is not yet supported, please configure the module to use a RESTful end point.'));
  }
}

/**
 * Submission hook for settings form 'Update session id' action.
 *
 * Delete session and perform a login request to Sugar using the new submitted
 * credentials.
 */
function sugarondrupal_admin_settings_update_session_submit($form, $form_state) {
  // Forget current session if existing.
  variable_set('sugarondrupal_session', NULL);

  /**
   * Get a new session value. The idea behind sugarondrupal_session_get() is to
   * make a new login request if there is no session stored, using the submitted
   * credentials.
   */
  $params = array(
    'user_auth' => array(
      'user_name' => $form_state['values']['sugarondrupal_user'],
      'password'  => md5($form_state['values']['sugarondrupal_pass']),
      'version'   => '1.0'
    ),
    'application_name' => 'SugarOnDrupal',
    'name_value_list' => array(),
  );
  $session = sugarondrupal_session_get($params);

  // Show a message to the adminsitrator.
  if ($session) {
    drupal_set_message(t('Got a valid session id. Both applications are now connected.'));
  }
  else {
    drupal_set_message(t('Unable to get a valid session, check credentials or connection settings.'), 'error');
  }
}

/**
 * Integration status information
 */

/**
 * SugarCRM and Drupal integration status page.
 *
 * Show information about SugarCRM and Drupal integration status to the site
 * administrator. The page is created as a form to allow other extension modules
 * to include additional information.
 *
 * @todo: rewrite this to make it use renderable arrays() instead of fixed
 * tables.
 *
 * @return
 *   A form showing current integration status.
 */
function sugarondrupal_admin_status($form, $form_state) {
  $form = array();

  /**
   * Generic SugarCRM server information.
   */
  $form['sugarondrupal_server'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('SugarCRM information'),
  );
  $rows = array();

  $info = sugarondrupal_get_server_info();
  $rows[] = array(t('SugarCRM version'), sugarondrupal_sugarcrm_version_string($info));
  if (isset($info['gmt_time'])) {
    $rows[] = array(t('GMT time'), $info['gmt_time']);
  }

  $form['sugarondrupal_server']['sugarondrupal_server_info'] = array(
    '#markup'          => theme('table', array('rows' => $rows)),
  );
  
  // From here on a session id is required to communicate with SugarCRM. Before
  //  continuing, verify that current session is still alive.
  if (!sugarondrupal_get_user_id()) {
    drupal_set_message(t('The session used to communicate Drupal with SugarCRM is invalid. Configure your SugarCRM connection or authenticate the session again in the !url page.', array('!url' => l(t("SugarCRM connection settings"), 'admin/config/sugarondrupal/settings'), '!uninstall' => l(t("uninstall the module"), 'admin/modules'))), 'error');
    return $form;
  }

  /**
   * SugarCRM connection information.
   */
  $form['sugarondrupal_conn'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('SugarCRM Connection'),
    '#description'    => t('The information shown below is related to the account used to initialize the connection bewteen Drupal and SugarCRM.')
  );
  $rows = array();

  $info = variable_get('sugarondrupal_session_info', array());
  $values = isset($info['name_value_list']) ? _sugarondrupal_name_value_list_to_array($info['name_value_list']) : array();
  
  if (count($values)) {
    // Deliberately hide the session Id.
    // $rows[] = array(t('Session id'), isset($info['id']) ? $info['id'] : '');
    $rows[] = array(t('Connected using entry'), _sugarondrupal_l($values['user_name'], "index.php?module=" . $info['module_name'] . "&action=DetailView&record=" . $values['user_id']));
    $rows[] = array(t('Language'), $values['user_language']);
    $rows[] = array(t('Currency'), $values['user_currency_name']);
  }
  else {
    $rows[] = array(t('No connection information found.'));
  }

  $form['sugarondrupal_conn']['sugarondrupal_conn_info'] = array(
    '#markup'          => theme('table', array('rows' => $rows)),
  );

  /**
   * Module information
   */
  $form['sugarondrupal_mod'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('SugarCRM Modules'),
    '#description'    => t('List of available modules.'),
  );
  $rows = array();

  $modules = sugarondrupal_get_available_modules();
  foreach ($modules as $id => $name) {
    $rows[] = array(l($name, 'admin/reports/status/sugarcrm/' . $name));
  }
  $form['sugarondrupal_mod']['sugarondrupal_modules'] = array(
    '#markup'          => theme('table', array('rows' => $rows)),
  );

  /**
   * Dummy placeholder
   */
  $form['sugarondrupal_placeholder'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('SugarCRM information placeholder'),
    '#description'    => t('Dummy status information TODO: Improve me.'),
  );

  return $form;
}

/**
 * Build a link to SugarCRM.
 *
 * Very alpha code, need some improvements or create a specific sugarondrupal
 * variable to hold the SugarCRM url.
 */
function _sugarondrupal_l($text, $path, $options = array()) {
  if (!$url = variable_get('sugarondrupal_webservice_endpoint', NULL)) {
    return NULL;
  }

  // Parse service url removing any service reference.
  $data = preg_split('/service\/v2/i', $url);
  $path = $data[0] . $path;

  return l($text, $path, $options);
}

/**
 * SugarCRM module information status page.
 *
 * Show information about...
 *
 * @param $name
 *   Module name to show information about.
 *
 * @return
 *   A form showing current integration status.
 */
function sugarondrupal_admin_module_status($form, $form_state, $name = NULL) {
  $form = array();
  
  // Go back to SugarCRM status information page if no Module is specified.
  if (!isset($name)) {
    drupal_goto('admin/reports/status/sugarcrm');
  }

  // Get all information available for this module.
  $params = array(
    'module_name' => $name,
    'fields'      => array(),
  );
  $info = sugarondrupal_get_module_fields($params);

  drupal_set_title(t('Information for module: !name', array('!name' => $name)));

  if (isset($info['number']) && $info['number'] == 20) {
    drupal_set_message(t('!message', array('!message' => isset($info['description']) ? $info['description'] : 'Module not available')), 'error');
    return $form;
  }

  /**
   * Module fields.
   */
  $form['sugarondrupal_fields'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('SugarCRM Module fields'),
    '#description'    => t('List of available fields for module: !module.', array('!module' => $info['module_name'])),
  );
  $rows = array();

  // @Todo: it may happen that module_fields is empty.
  foreach ($info['module_fields'] as $id => $field) {
    $rows[] = array(
      isset($field['label']) ? $field['label'] : '',
      isset($field['name']) ? $field['name'] : '',
      isset($field['type']) ? $field['type'] : '',
      isset($field['required']) ? $field['required'] : '',
    );
  }
  $header = array(
    t('Label'),
    t('Name'),
    t('Type'),
    t('Required'),
  );
  $form['sugarondrupal_fields']['sugarondrupal_module_fields'] = array(
    '#markup'          => theme('table', array('header' => $header, 'rows' => $rows)),
  );

  /**
   * Module relations.
   */
  $form['sugarondrupal_rela'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('SugarCRM Module Relations'),
    '#description'    => t('List of available relations for module: !module.', array('!module' => $info['module_name'])),
  );
  $rows = array();

  // @Todo: it may happen that link_fields is empty.
  foreach ($info['link_fields'] as $id => $link) {
    $rows[] = array(
      isset($link['name']) ? $link['name'] : '',
      isset($link['module']) ? $link['module'] : '',
      isset($link['type']) ? $link['type'] : '',
      isset($link['relationship']) ? $link['relationship'] : '',
      isset($link['bean_name'])? $link['bean_name'] : '',
    );
  }
  $header = array(
    t('Name'),
    t('Module'),
    t('Type'),
    t('Relationship'),
    t('Bean Name'),
  );
  $form['sugarondrupal_rela']['sugarondrupal_relation_fields'] = array(
    '#markup'          => theme('table', array('header' => $header, 'rows' => $rows)),
  );

  return $form;
}

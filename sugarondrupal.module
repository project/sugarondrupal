<?php

/**
 * @file
 * Main conector for third party integration with SugarCRM
 *
 * This module creates a service link between Drupal and SugarCRM using SugarCRM
 * services Version 2. This module enables the creation of complex integration
 * schemas by providing an API to read and update information on the SugarCRM
 * site from the Drupal site.
 *
 * For the communication between both applications, a REST interface using JSON
 * encoding is called in SugarCRM from Drupal. The module handles the Endpoint
 * location and authentication request required to perform all the subsequent
 * service method request.
 *
 * @TODO: Write documentation on how to configure the soap or REST user.
 * @TODO: Write documentation about the flow of the WebService requests
 */

/**
 * Implementation of hook_permission().
 */
function sugarondrupal_permission() {
  return array(
    'administer SugarCRM settings' => array(
      'title'       => t('Administer SugarCRM settings'),
      'description' => t('Perform administration tasks for SugarCRM and Drupal integration.'),
    )
  );
}

/**
 * Implementation of hook_menu().
 */
function sugarondrupal_menu() {
  $items = array();

  $items['admin/config/sugarondrupal'] = array(
    'title'             => 'SugarCRM Integration',
    'description'       => 'Manage your Drupal to SugarCRM integration',
    'position'          => 'right',
    'weight'            => -10,
    'page callback'     => 'sugarondrupal_system_admin_menu_block_page',
    'access arguments'  => array('access administration pages'),
    'file'              => 'sugarondrupal.admin.inc',
  );

  $items['admin/config/sugarondrupal/settings'] = array(
    'title'             => 'Connection settings',
    'description'       => 'Manage Drupal to SugarCRM integration settings, like SugarCRM location, credentials and networking options.',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('sugarondrupal_admin_settings'),
    'access arguments'  => array('administer SugarCRM settings'),
    'file'              => 'sugarondrupal.admin.inc',
    'weight'            => -10,
  );

  $items['admin/reports/status/sugarcrm'] = array(
    'title'             => 'SugarCRM Information',
    'description'       => 'View Drupal to SugarCRM integration status',
    'type'              => MENU_CALLBACK,
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('sugarondrupal_admin_status'),
    'access arguments'  => array('administer SugarCRM settings'),
    'file'              => 'sugarondrupal.admin.inc',
  );
  $items['admin/reports/status/sugarcrm/%'] = array(
    'title'             => 'SugarCRM Module Information',
    'description'       => 'View detailled SugarCRM module information',
    'type'              => MENU_CALLBACK,
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('sugarondrupal_admin_module_status', 4),
    'access arguments'  => array('administer SugarCRM settings'),
    'file'              => 'sugarondrupal.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_help().
 */
function sugarondrupal_help($path, $arg) {
  $help = '';

  // Return appropiate help according to the path
  switch ($path) {
    // Main module help for the block module
    case 'admin/help#sugarondrupal':
      $help .= t('<p>SugarOnDrupal communicates Drupal and SugarCRM sites.</p>');
      break;
    case 'admin/config/sugarondrupal':
      $help .= t('<p>The SugarOnDrupal package may include several modules. Below you will find a list of available administration links for your Drupal and SugarCRM integration.</p>');
      break;
    case 'admin/config/sugarondrupal/settings':
      $help .= t('<p>The SugarOnDrupal settings page allow the configuration of SugarCRM communication and authentication options.</p>');
      break;
    case 'admin/reports/status/sugarcrm':
      $help .= t('<p>The information shown in this page has been requested to the SugarCRM EndPoint.</p>');
      break;
  }

  return $help;
}

/**
 * Implementation of hook_cron().
 *
 * This hook is implemented to keep the SugarCRM session alive by performing a
 * method request requiring a valid session id.
 */
function sugarondrupal_cron() {

  /**
   * Verify that session is still active requesting the user ID of the session.
   * If there is no valid reply, then remove current session and session info
   * from the module variables, so the module can try to login again the next 
   * time a request is submitted.
   */
  if (!sugarondrupal_get_user_id()) {
    variable_del('sugarondrupal_session');
    variable_del('sugarondrupal_session_info');
  }
}

/**
 * SugarCRM Action definition
 */

/**
 * Implementation of hook_action_info().
 */
function sugarondrupal_action_info() {
  return array(
    'sugarondrupal_set_entry_action' => array(
      'label'         => t('Create a SugarCRM entry'),
      'type'          => 'system',
      'configurable'  => TRUE,
      'triggers'      => array('any'),
    ),
  );
}

/**
 * Configure SugarCRM Set Entry advanced action.
 *
 * @param $context array for context of this advanced action.
 *
 * @return
 *   a configuration Form array for the advanced action.
 */
function sugarondrupal_set_entry_action_form($context) {
  $form = array();

  // Make $context available for hook_form_alter functions.
  $form['sugarondrupal_context'] = array(
    '#type'        => 'value',
    '#value'       => $context,
  );

  // Make the user select one module.
  $modules = sugarondrupal_get_available_modules();
  foreach ($modules as $name) {
    $module_names[$name] = $name;
  }
  $form['sugarondrupal_module_name'] = array(
    '#type'           => 'select',
    '#title'          => t('Entry type'),
    '#options'        => $module_names,
    '#description'    => t('Select the entry type to be created in SugarCRM. Please, note that only one entry of one type will be created. If you want to provide a Contact with custom Account (a different module information) you need to use additional SugaronDrupal configuration options.'),
    '#default_value'  => isset($context['sugarondrupal_module_name']) ? $context['sugarondrupal_module_name'] : '',
    '#weight'         => -1,
  );

  return $form;
}

/**
 * Implementation of hook_form_system_actions_configure_alter().
 */
function sugarondrupal_form_system_actions_configure_alter(&$form, $form_state) {
  drupal_set_message("<pre>". print_r($form_state,1)."</pre>");
  // Only process sugarondrupal advanced actions.
  if (!isset($form['sugarondrupal_module_name'])) {
    return; 
  }

  // Retrieve advanced action context.
  $context = $form['sugarondrupal_context']['#value'];

  if (isset($form_state['input']['sugarondrupal_module_name']) || !empty($form['sugarondrupal_module_name']['#default_value'])) {
    // sugarondrupal_module_name field might not be set yet, so use POST information.
    $params = array(
      'module' => (isset($form['sugarondrupal_module_name']['#default_value']) && !empty($form['sugarondrupal_module_name']['#default_value'])) ? $form['sugarondrupal_module_name']['#default_value'] : $form_state['input']['sugarondrupal_module_name'],
      'fields' => array(),
    );
    $fields = sugarondrupal_get_module_fields($params);

    // Create a container.
    $form['sugarondrupal_fields'] = array(
      '#type'   => 'fieldset',
      '#title'  => t('Entry fields'),
      '#weight' => 0,
    );

    // Provide some help about tokens.
    $form['sugarondrupal_fields']['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    if (module_exists('token')) {
      $form['sugarondrupal_fields']['token_help']['help'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('all'),
      );
    }
    else {
      $form['sugarondrupal_fields']['token_help']['help'] = array(
        '#markup' => t('You may use Drupal predefined tokens here. There is a list of available tokens in the !url. Depending on the object originating the action, only a few tokens might be available.', array('!url' => l(t('Token list handbook page'), 'http://drupal.org/node/390482'))),
      );
      $form['sugarondrupal_fields']['token_help']['#collapsed'] = FALSE;
    }

    // Attach SugarCRM fields.
    foreach ($fields['module_fields'] as $name => $field) {
      $form['sugarondrupal_fields'][$name] = _sugarondrupal_build_form_element($field, $context);
    }
  }
  // The form has not been submitted, make initial alterations.
  else {
    $form['actions']['submit']['#value'] = t('Continue');
  }

  return $form;
}

function _sugarondrupal_build_form_element($field, $context) {
  $element = array();

  // Convert SugarCRM entry fields into Drupal Form API fields.
  switch ($field['type']) {
    case 'relate':
      return NULL;
    case 'name':
    case 'varchar':
    case 'full_name':
    case 'phone':
    case 'email':
      $element['#type'] = 'textfield';
      break;
    case 'datetime':
      $element['#type'] = 'textfield';
      $element['#description'] = t('Use SugarCRM datetime format.');
      break;
    case 'date':
      $element['#type'] = 'textfield';
      $element['#description'] = t('Use SugarCRM date format.');
      break;
    case 'assigned_user_name':
      $element['#type'] = 'textfield';
      $element['#description'] = t('Use SugarCRM assigned user name format.');
      break;
    case 'id':
      $element['#type'] = 'textfield';
      $element['#description'] = t('Use SugarCRM id format.');
      break;
    case 'text':
      $element['#type'] = 'textarea';
      break;
    case 'bool':
      $element['#type'] = 'checkbox';
      break;
    case 'enum':
      $element['#type'] = 'select';
      $element['#options'] = _sugarondrupal_name_value_list_to_array($field['options']);
      break;
  }

  $element['#title']    = str_replace(':', '', $field['label']);
  $element['#name']     = $field['name'];
  $element['#required'] = $field['required'];
  $element['#default_value'] = isset($context['sugarondrupal_module_fields'][$field['name']]) ? $context['sugarondrupal_module_fields'][$field['name']] : NULL;

  // Avoid special case for entry ID field that is required. This is only
  // required for entry updates.
  if ($field['name'] == 'id') {
    $element['#required'] = FALSE;
  }

  return $element;
}




/**
 * Validation handler for SugarCRM Set Entry advanced action configuration form.
 */
function sugarondrupal_set_entry_action_validate($form, $form_state) {
  // Create a multistep process in this form.
  if ($form_state['values']['op'] == t('Continue')) {
    form_set_error('', '');
  }
}

/**
 * Submit handler for SugarCRM Set Entry advanced action configuration form.
 */
function sugarondrupal_set_entry_action_submit($form, $form_state) {
  $context = array();

  // Save the SugarCRM module that will hold the new entry.
  $context['sugarondrupal_module_name'] = $form_state['values']['sugarondrupal_module_name'];

  // Save the fields that will be populated in the new entry.
  foreach ($form['sugarondrupal_fields'] as $name => $field) {
    if (!empty($form_state['values'][$name])) {
      $context['sugarondrupal_module_fields'][$name] = $form_state['values'][$name];
    }
  }

  return $context;
}

/**
 * Execute SugarCRM Set Entry advanced action.
 *
 * @param $object any of User, Node, Comment or system.
 * @param $context array of configuration for this advanced action.
 */
function sugarondrupal_set_entry_action(&$entity, $context) {
  $type = 'global';

  // Guess entry type by hook and set object to actual object.
  // @todo: pretty stright update from D6 to D7, requires recoding.
  switch ($context['group']) {
    case 'node':
      $object = $context['node'];
      $type = 'node';
      break;
    case 'user':
      $object = $context['user'];
      $type = 'user';
      break;
    case 'comment':
      $object = $context['comment'];
      $type = 'comment';
      break;
    case 'taxonomy':
      $type = 'taxonomy';
      break;
  }

  // Perform entry fields replacement.
  $fields = array();
  foreach ($context['sugarondrupal_module_fields'] as $name => $value) {
    $fields[$name] = token_replace($value, $context);
  }

  // Submit the entry to SugarCRM
  $params = array(
    'module'          => $context['sugarondrupal_module_name'],
    'name_value_list' => _sugarondrupal_array_to_name_value_list($fields),
  );
  sugarondrupal_set_entry($params);
}


/**
 * Perform very primitive [token] substitution.
 */
function sugarondrupal_replace_token($value, $object) {
  if (preg_match('/\[(.*)\]/', $value, $matches)) {
    $attrib = $matches[1];
    $value = (is_object($object) && isset($object->$attrib)) ? $object->$attrib : $value;
  }

  return $value;
}


/**
 * SugarOnDrupal helper functions.
 */

/**
 * Build a SugarCRM version string.
 *
 * @return
 *   human readable SugarCRM version string.
 */
function sugarondrupal_sugarcrm_version_string($info = NULL) {
  // Default known SugarCRM flavors.
  $flavors = array(
    'ENT' => t('SugarCRM Enterprise'),
    'PRO' => t('SugarCRM Professional'),
    'CE'  => t('SugarCRM Community Edition'),
  );

  // Use passed information or get from the configured SugarCRM Web Service.
  $info = isset($info) ? $info : sugarondrupal_get_server_info();

  if (isset($info['flavor']) && isset($info['version'])) {
    $info['flavor'] = isset($flavors[$info['flavor']]) ? $flavors[$info['flavor']] : $info['flavor'];
    $version_string = (isset($info['version']) && isset($info['flavor'])) ? t('!flavor !version', array('!flavor' => $info['flavor'], '!version' => $info['version'])) : t('Not identified.');
  }
  else {
    $version_string = t('Not found');
  }

  return $version_string;
}

/**
 * Get the session id or create a new one if there is no session.
 *
 * @todo: verify that session is valid (this can overload the service with
 * several requests in a single page load.
 *
 * @param $params
 *   associative array of service parameters, it may include 'session' parameter
 *   that will superseed current saved session value.
 * @return
 *   the session id or NULL if no session.
 */
function sugarondrupal_session_get($params = array()) {
  // Use passed session first, fallback to current saved session.
  $session = isset($params['session']) ? $params['session'] : variable_get('sugarondrupal_session', NULL);

  // Try to get a new session if there is a password stored or authentication
  // information has been passed as part of the params array.
  if (( isset($params['user_auth']) || variable_get('sugarondrupal_pass', NULL) )&& !$session) {
    // Record that a new session id has been generated.
    if ($session = sugarondrupal_login_request($params)) {
      watchdog('sugarondrupal', 'Got new SugarCRM session id.');
    }
  }

  return $session;
}

//API
//
//@TODO: BE SURE THAT ERRORS ARE POPULATED BACKWARDS IN STACKED CALLS
/*
 * Get the connection object or create one if not exists
 */

/**
 * Implementation of get_user_id sugarcrm webservice
 *
 * @param $params associative array of request parameters.
 *
 * @return
 *   an ID of the current logged in user (using the session id) or NULL.
 */
function sugarondrupal_get_user_id($params = array()) {
  // Use existing session id if not specified.
  $params += array(
    'session' => sugarondrupal_session_get(),
  );

  $result = sugarondrupal_sugarcrm_service_request('get_user_id', $params);

  return isset($result['id']) ? $result['id'] : NULL;
}

/**
 * Implementation of get_available_modules sugarcrm webservice
 *
 * @param $params associative array of request parameters.
 *
 * @return
 *   an array with the available modules.
 */
function sugarondrupal_get_available_modules($params = array()) {
  // Use existing session id if not specified.
  $params += array(
    'session' => sugarondrupal_session_get(),
  );

  $result = sugarondrupal_sugarcrm_service_request('get_available_modules', $params);

  return isset($result['modules']) ? $result['modules'] : NULL;
}

/**
 * Return SugarCRM version information from Endpoint.
 *
 * @return
 *   associative array of SugarCRM information. Currently the following fields
 *   are returned by the web service:
 *   - flavor
 *   - version
 */
function sugarondrupal_get_server_info($params = array()) {
  // This is an unauthenticated method. There is no need for a session to make
  // this webservice method call.
  return sugarondrupal_sugarcrm_service_request('get_server_info', $params);
}

/*
 * Implementation of set_entry sugarcrm webservice
 */
function sugarondrupal_set_entry($params) {
  //we provide the session element of the params
  $params = array_merge(array('session' => sugarondrupal_session_get()), $params);
  $result = sugarondrupal_sugarcrm_service_request('set_entry', $params);
  return $result;
}

/*
 * Implementation of get_entry sugarcrm webservice
 */
function sugarondrupal_get_entry($params) {
  //we provide the session element of the params
  $params = array_merge(array('session' => sugarondrupal_session_get()), $params);
  $result = sugarondrupal_sugarcrm_service_request('get_entry', $params);
  //RESTful returns name_value_list as [name] => value,
  //SOAP returns name_value_list as [] => array('name' => name, 'value' => value
  //Convert SOAP to RESTful format
  if (variable_get('sugarondrupal_webservice_protocol', 'rest') == 'soap') {
    if ((count($result['entry_list']))) {
      foreach ($result['entry_list'] as $id => $element) {
        $result['entry_list'][$id]['name_value_list'] = name_value_list_to_array($element['name_value_list']);
      }
    }
  }
  return $result;
}

/*
 * Implementation of get_entry sugarcrm webservice
 * SugarCRM doesn't create any error if the entry id doesn't exist, so we should
 * care about the elements returned in return['entry_list']
 */
function sugarondrupal_get_entry_list($params) {
  //we provide the session element of the params
  $params = array_merge(array('session' => sugarondrupal_session_get()), $params);
  $result = sugarondrupal_sugarcrm_service_request('get_entry_list', $params);
  //RESTful returns name_value_list as [name] => value,
  //SOAP returns name_value_list as [] => array('name' => name, 'value' => value
  //Convert SOAP to RESTful format
  if (variable_get('sugarondrupal_webservice_protocol', 'rest') == 'soap') {
    if ((count($result['entry_list']))) {
      foreach ($result['entry_list'] as $id => $element) {
        $result['entry_list'][$id]['name_value_list'] = name_value_list_to_array($element['name_value_list']);
      }
    }
  }
  return $result;
}

/*
 * Implementation of get_entry sugarcrm webservice
 * SugarCRM doesn't create any error if the entry id doesn't exist, so we should
 * care about the elements returned in return['entry_list']
 */
function sugarondrupal_get_entries_count($params) {
  //we provide the session element of the params
  $params = array_merge(array('session' => sugarondrupal_session_get()), $params);
  $result = sugarondrupal_sugarcrm_service_request('get_entries_count', $params);

  return $result;
}


/*
 * Implementation of get_module_fields sugarcrm webservice
 */
function sugarondrupal_get_module_fields($params) {
  //we provide the session element of the params
  $params = array_merge(array('session' => sugarondrupal_session_get()), $params);
  $result = sugarondrupal_sugarcrm_service_request('get_module_fields', $params);
  //From soap, each module_field element of array is indexed, [0] [1], and from
  //RESTful, they are named [id] [name]..
  if (variable_get('sugarondrupal_webservice_protocol', 'rest') == 'soap') {
    if ((count($result['module_fields']))) {
      $fields = array();
      foreach ($result['module_fields'] as $id => $element) {
        $fields[$element['name']] = $element;
      }
      $result['module_fields'] = $fields;
    }
  }
  return $result;
}






/**
 * Internal Web service consolidation.
 *
 * SOAP and REST interface, and V1 and V2 versions return different objects
 * We should consolidate all of the available options into a  single format.
 */


/*
 * keep the session_id variable updated doing a webservice login.
 * 
 * Saves Service response information in a module variable.
 *
 */
function sugarondrupal_login_request($params = array()) {
  $session = NULL;

  // Perform session information clean up.
  variable_del('sugarondrupal_session');
  variable_del('sugarondrupal_session_info');

  // Complete missing parameters with default saved information.
  $params += array(
    'user_auth' => array(
      'user_name' => variable_get('sugarondrupal_user', ''),
      'password'  => md5(variable_get('sugarondrupal_pass', '')),
      'version'   => '1.0'
    ),
    'application_name' => 'SugarOnDrupal',
    'name_value_list' => array(),
  );

  $result = sugarondrupal_sugarcrm_service_request('login', $params);
  if (isset($result['id'])) {
    $session = $result['id'];

    // Save current login information.
    variable_set('sugarondrupal_session_info', $result);
  }
  variable_set('sugarondrupal_session', $session);

  return $session;
}

/*
 *
 * 
 * 
 * LOW LEVEL WebService API
 *
 *
 */

/*
 * Main handler of a webservice request to SugarCRM.
 *
 * An special parameter called 'sugarondrupal_skip_request' can be set by other
 * modules to prevent the method call from being executed.
 * 
 * @param $method
 *   A string with the method of the webservice to be called
 * @param $params
 *   An array containing an method arguments name => value pair.
 * @return
 *   result of the $method API call. Probably an array or NULL
 *
 * @TODO: consolidate V1 and V2 requests
 *
 */
function sugarondrupal_sugarcrm_service_request($method, $params = array()) {
  $result = array();

  // Return if no endpoint defined.
  if (!$url = variable_get('sugarondrupal_webservice_endpoint', NULL)) {
    return NULL;
  }

  // Perform alterations on parameters before being used in the service request.
  drupal_alter('sugarcrm_service_request', $params, $method);
  drupal_alter('sugarcrm_service_' . $method . '_request', $params);

  // Record debug information
  if (variable_get('sugarondrupal_debug', FALSE) == TRUE) {
    $message = '!method request => !params';
    watchdog('sugarondrupal', $message, array('!method' => $method, '!params' => print_r($params, 1)));
  }

  if (!isset($params['sugarondrupal_skip_request'])) {
    if (variable_get('sugarondrupal_webservice_protocol', 'rest') == 'soap') {
      $result = _sugarondrupal_soap_request($url, $method, $params);
    }
    else {
      // Unless soap specified, safely run REST call
      $result = _sugarondrupal_rest_request($url, $method, $params);
    }
  }

  // Record debug information
  if (variable_get('sugarondrupal_debug', FALSE) == TRUE) {
    $message = '!method response => !response';
    watchdog('sugarondrupal', $message, array('!method' => $method, '!response' => print_r($result, 1)));
  }

  // Perform alterations on results before being used in the service requester.
  drupal_alter('sugarcrm_service_respose', $result, $method, $params);
  drupal_alter('sugarcrm_service_' . $method . '_response', $result, $params);

  return $result;
}


/*
 * LOW LEVEL SOAP API: SOAP call
 *
 * @TODO: To be coded
 */

/*
 * Dummy implementation
 */
function _sugarondrupal_soap_request($url, $method, $params = array()) {
  // Check SOAP connection.. Don't care about account, just test wsdl endpoint
  $options = array(
    'namespace' => '',
    'use'       => 'literal',
    'style'     => 'rpc',
  );
  $client = soapclient_init_client($url, TRUE, $options);

  if ( $client['#error'] == FALSE ) {
    $result =  $client['#return']->call($method, $params);
    return $result['#return'];
  }
  return NULL;
}



/*
 * LOW LEVEL REST API: REST call
 *
 * @TODO: Handle errors here
 */


/*
 * Performs a REST request to the SugarCRM service
 *
 * "json" is the default encoding and is provided as compat when it is not
 * enabled in the server. SugarCRM 5.5.Beta1 and SugarCRM 6.1.0 do not work in
 * seralize mode.
 *
 * @param $url
 *   A string containing a fully qualified URI of the Rest service.
 * @param $method
 *   A string with the method of the webservice to be called
 * @param $params
 *   An array containing an method arguments name => value pair.
 * @return
 *   result of the $method API call. Probably an array or NULL
 *
 * @TODO: currently, sugarondrupal_rest_encoding is not initialized anywhere
 */
function _sugarondrupal_rest_request($url, $method, $params = array()) {

  /**
   * Prepare drupal_http_request options for this interface. Some variables are
   * not controlled by this module, but may be used to alter the behavour of the
   * Drupal HTTP client.
   */
  // Include variables defined by other modules.
  $headers = variable_get('sugarondrupal_http_headers', array());

  // Set HTTP content type to correct encoding.
  $headers += array(
    'Content-Type' => 'application/x-www-form-urlencoded'
  );

  // Verify that http does require authentication to include the header.
  $http_user = variable_get('sugarondrupal_http_user', '');
  $http_pass = variable_get('sugarondrupal_http_pass', '');
  if (!empty($http_user) && !empty($http_pass)) {
    $headers += array(
      'Authorization' => 'Basic ' . base64_encode($http_user . ':' . $http_pass),
    );
  }

  // Setup drupal_http_request connection headers.
  $options = array(
    'timeout'       => floatval(variable_get('sugarondrupal_http_timeout',   30)),
    'max_redirects' => intval(variable_get('sugarondrupal_http_redirects',  3)),
    'context'       => variable_get('sugarondrupal_http_context', NULL),
    'headers'       => $headers,
  );

  /**
   * Submit the appropiate HTTP request depending on the encoding type. For JSON
   * the returned object must be converted to array.
   */
  if (variable_get('sugarondrupal_rest_encoding', 'JSON') == 'JSON') {
    //remember to use _sugarondrupal_object_to_array to return array
    $data = _sugarondrupal_rest_json_request($url, $method, $params, $options);
    $result = _sugarondrupal_object_to_array($data);
  }
  else {
    //Safely use serialize if json is not specified
    $result = _sugarondrupal_rest_serialize_request($url, $method, $params, $options);
  }

  // Try to identify and record protocol errors by name or error.
  if (is_array($result) && isset($result['name']) && isset($result['number'])) {
    $variables = array(
      '!name'   => isset($result['name']) ? $result['name']   : t('Unknown error.'),
      '!number' => isset($result['number']) ? $result['number'] : t('Unkown'),
      '!desc'   => isset($result['description']) ? $result['description'] : t('Unknown error.'),
    );

    watchdog('sugarondrupal', '!name (!number): !desc', $variables, WATCHDOG_ERROR);
  }

  return $result;
}

/*
 * Performs a serialized REST request of $method using $params
 *
 * As SugarCRM handles $_REQUEST variable, it could be done with GET
 *
 * @param $url
 *   A string containing a fully qualified URI of the Rest service.
 * @param $method
 *   A string with the method of the webservice to be called
 * @param $params
 *   An array containing an method arguments name => value pair.
 * @param $options
 *   Additional drupal_http_request options array.
 * @return
 *   result of the $method API call. Probably an array or NULL
 *
 * @TODO: allow POST request
 */
function _sugarondrupal_rest_serialize_request($url, $method, $params, $options) {

  // Build the submit body.
  $request  = array(
    'method'        => $method,
    'input_type'    => 'SERIALIZE',
    'response_type' => 'SERIALIZE',
    'rest_data'     => serialize($params),
  );

  // Submit http request.
  $options += array(
    'method'  => 'POST',
    'data'    => http_build_query($request, '', '&'),
  );
  $output = drupal_http_request($url, $options);

  return isset($output->data) ? unserialize($output->data) : NULL;
}

/*
 * Performs a JSON REST request of $method using $params
 *
 * As SugarCRM handles $_REQUEST variable, it could be done with GET
 *
 * @param $url
 *   A string containing a fully qualified URI of the Rest service.
 * @param $method
 *   A string with the method of the webservice to be called
 * @param $params
 *   An array containing an method arguments name => value pair.
 * @param $options
 *   Additional drupal_http_request options array.
 * @return
 *   result of the $method API call. Probably a stdClass or NULL
 *
 * @TODO: allow POST request
 */
function _sugarondrupal_rest_json_request($url, $method, $params, $options) {
  // provide php < 5.2 json compatibility
  module_load_include('php', 'sugarondrupal', 'compat_json');

  // Build the submit body.
  $request  = array(
    'method'        => $method,
    'input_type'    => 'JSON',
    'response_type' => 'JSON',
    'rest_data'     => json_encode($params),
  );

  // Submit http request.
  $options += array(
    'method'  => 'POST',
    'data'    => http_build_query($request, '', '&'),
  );
  $output = drupal_http_request($url, $options);

  return isset($output->data) ? json_decode($output->data) : NULL;
}


/**
 * Helper functions
 *
 * SugarCRM uses several variable types. These functions are helpers to manage
 * the SugarCRM information and conform it betwen different API and encoding
 * versions.
 */

/**
 * Convert (non-recursive) Name => Value pair lists to associative array.
 *
 * JSON enconding returns SugarCRM entry information in a special format called
 * Name value list. This is an example:
 * array(
 *   'field_name' => array(
 *     'name'  => 'field_name',
 *     'value' => 'value',
 *   )
 * )
 *
 * This function returns the information in an associative array format.
 *
 * @param $name_value_list
 *   the name value array list.
 * @return
 *   an associative array of the name value list items.
 */
function _sugarondrupal_name_value_list_to_array($name_value_list) {
  $items = array();

  // Just in case of failing input.
  if (!is_array($name_value_list)) {
    return $items;
  }

  // Transform the pairs to array.
  while (list($name, $data) = each($name_value_list)) {
    if (isset($data['name'])) {
      $items[$data['name']] = isset($data['value']) ? $data['value'] : NULL;
    }
  }

  return $items;
}

/**
 * Convert (non-recursive) associative array to Name => Value pair lists.
 *
 * SugarCRM uses Name-value list to set information in the following format:
 * array(
 *   array(
 *     'name'  => 'field_name',
 *     'value' => 'value',
 *   )
 * )
 *
 * This function returns a name-value list built with input array attributes.
 *
 * @param $array
 *   the associative array to convert.
 * @return
 *    a name value list array.
 */
function _sugarondrupal_array_to_name_value_list($array) {
  $items = array();

  foreach ($array as $name => $value) {
    $items[] = array(
      'name'  => $name,
      'value' => $value,
    );
  }

  return $items;
}

/**
 * Recursively converts and StdClass object to associative array.
 *
 * @param $object
 *   the StdClass object to convert.
 * @return
 *   associative array of StdClass attributes and their value.
 */
function _sugarondrupal_object_to_array($object) {
  $array = array();

  if (is_object($object) || is_array($object)) {
    foreach ($object as $key => $value) {
      $array[$key] = _sugarondrupal_object_to_array($value);
    }
  }
  else {
    $array = $object;
  }

  return $array;
}
